package com.minecraftonline.minecraftgift;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.util.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class WeightedItemDeterminer implements ItemDeterminer {

    // Declare a random
    private final Random rand = new Random();

    // Picking items out of a hat
    private int hatContent = 0;

    private final List<Tuple<ItemStack, Integer>> weightedList = new ArrayList<>();

    public static final int DEFAULT_WEIGHT = 10;

    public void add(ItemStack itemStack, int weight) {
        if (weight < 1) {
            throw new IllegalArgumentException("Weight less than one.");
        }
        hatContent += weight;
        weightedList.add(Tuple.of(itemStack, weight));
    }

    public void add(ItemStack item) {
        add(item, DEFAULT_WEIGHT);
    }

    public void add(ItemType item, int weight) {
        add(ItemStack.of(item), weight);
    }

    public void add(ItemType item) {
        add(item, DEFAULT_WEIGHT);
    }

    public int getHatContent() {
        return hatContent;
    }

    // Attempt to get the probabilities for the items in the list.
    public String getProbabilities() {
        // Text output = Text.builder().build();
        String output = giftListName() + ", probability (percentage)\n";
        float hat = (float) getHatContent();
        for (Tuple<ItemStack, Integer> tuple : weightedList) {
            // percentage...multiply by a hundred.
            float probability = ((float) tuple.getSecond() / hat) * 100;
            Text currentOutput = Text.builder()
                    .append(
                            Text.of(tuple.getFirst()
                                    .get(Keys.DISPLAY_NAME)
                                    .orElse(Text.of(tuple.getFirst().getType().getName()))))
                    .append(Text.of(", "))
                    .append(Text.of(probability))
                    .append(Text.NEW_LINE)
                    .build();
            // log.info(currentOutput.toPlain());
            output += currentOutput.toPlain();
        }
        return output;
    }


    @Override
    public ItemStack itemToGive() {
        // MinecraftGift.getLogger().info("Hat size is: " + hatContent);
        int randomInteger = rand.nextInt(hatContent);
        for (Tuple<ItemStack, Integer> tuple : weightedList) {
            randomInteger -= tuple.getSecond();
            if (randomInteger < 0) {
                return tuple.getFirst();
            }
        }
        throw new IllegalStateException("Did not find item in list to give with random " + randomInteger);
    }
}
