package com.minecraftonline.minecraftgift.potionlists;

import com.minecraftonline.minecraftgift.WeightedItemDeterminer;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.potion.PotionType;
import org.spongepowered.api.item.potion.PotionTypes;

public class ValentinesPotionList extends WeightedItemDeterminer {

    public ValentinesPotionList() {
        addPotion(PotionTypes.HEALING);
        addPotion(PotionTypes.STRONG_HEALING);

        addPotion(PotionTypes.REGENERATION);
        addPotion(PotionTypes.STRONG_REGENERATION);
        addPotion(PotionTypes.LONG_REGENERATION);
    }

    private void addPotion(PotionType potionType) {
        add(ItemStack.builder().itemType(ItemTypes.POTION).add(Keys.POTION_TYPE, potionType).build(), DEFAULT_WEIGHT * 5);
        add(ItemStack.builder().itemType(ItemTypes.SPLASH_POTION).add(Keys.POTION_TYPE, potionType).build(), DEFAULT_WEIGHT * 3);
        add(ItemStack.builder().itemType(ItemTypes.LINGERING_POTION).add(Keys.POTION_TYPE, potionType).build(), DEFAULT_WEIGHT);
    }
}
