package com.minecraftonline.minecraftgift.giftlists;

import com.minecraftonline.minecraftgift.WeightedItemDeterminer;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DoublePlantTypes;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.data.type.TreeTypes;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Color;
import org.spongepowered.api.world.gen.populator.DoublePlant;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class ValentinesGiftList extends WeightedItemDeterminer {

    // Not presently needed
    // private static final int MAP_ID = -1;

    private static final Color SPECIAL_RED = Color.ofRgb(199, 43, 80);
    private static int year = Calendar.getInstance().get(Calendar.YEAR);
    private static final List<Text> VALENTINES_DAY_LORE = Arrays.asList(Text.of(TextColors.GRAY, "Valentine's Day ", year));

    private static final String GIFT_LIST_NAME = "Valentines";

    private static final Text handoutText = Text.of(TextColors.GREEN, "Hourly ",
            TextColors.LIGHT_PURPLE, "Valentine's",
            TextColors.GREEN, " random gift handout! Only at MinecraftOnline!"
    );

    // feel sm0rt
    public static List<Text> getValentinesDayLore() { return VALENTINES_DAY_LORE; }

    @Override

    public String giftListName() { return GIFT_LIST_NAME; }

    public Text giftHandoutText() {
        return handoutText;
    }

    public ValentinesGiftList() {
        final ItemType[] armorItems = {
                ItemTypes.LEATHER_HELMET, ItemTypes.LEATHER_CHESTPLATE, ItemTypes.LEATHER_LEGGINGS, ItemTypes.LEATHER_BOOTS
        };


        // yes, unnecessary and inefficient, but easier with the items that you can't actually change the types of
        final ItemType[] woodenItems = {
                // This has a funny effect with jungle boats, not as intended. ItemTypes.JUNGLE_BOAT,
                ItemTypes.JUNGLE_DOOR, ItemTypes.JUNGLE_FENCE, ItemTypes.JUNGLE_FENCE_GATE, ItemTypes.JUNGLE_STAIRS,
                ItemTypes.PLANKS, ItemTypes.LOG, ItemTypes.WOODEN_SLAB,
        };

        for (ItemType i : woodenItems) {
            add(ItemStack.builder()
                    .itemType(i)
                    .add(Keys.TREE_TYPE, TreeTypes.JUNGLE)
                    .build());
        }

        // Deal with the above boat issue manually
        add(ItemTypes.JUNGLE_BOAT);

        final ItemType[] itemsToColor = {
                ItemTypes.CARPET, ItemTypes.DYE, ItemTypes.BED, ItemTypes.CONCRETE, ItemTypes.CONCRETE_POWDER,
                ItemTypes.STAINED_HARDENED_CLAY, ItemTypes.STAINED_GLASS, ItemTypes.STAINED_GLASS_PANE, ItemTypes.WOOL,
                ItemTypes.BANNER,
        };
        add(ItemTypes.PINK_GLAZED_TERRACOTTA);

        for (ItemType i : itemsToColor) {
            add(ItemStack.builder()
                    .itemType(i)
                    .add(Keys.DYE_COLOR, DyeColors.PINK)
                    .build());
        }

        for (ItemType i : armorItems) {
            add(ItemStack.builder()
                    .itemType(i)
                    .add(Keys.COLOR, SPECIAL_RED)
                    .add(Keys.ITEM_LORE, VALENTINES_DAY_LORE)
                    .build());
        }

        add(ItemTypes.RED_FLOWER);

        // At Anna's request
        // add(ItemTypes.DOUBLE_PLANT);
        // (Do it more properly)
        add(ItemStack.builder()
                .itemType(ItemTypes.DOUBLE_PLANT)
                .add(Keys.DOUBLE_PLANT_TYPE, DoublePlantTypes.ROSE)
                .build(),
                // weightier!
                DEFAULT_WEIGHT * 3);

        add(ItemTypes.REDSTONE_BLOCK);
        add(ItemTypes.REDSTONE);

        // Cocoa beans
        add(ItemStack.builder()
                .itemType(ItemTypes.DYE)
                .add(Keys.DYE_COLOR, DyeColors.BROWN)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.DARK_PURPLE, "Chocolates"))
                .add(Keys.ITEM_LORE, VALENTINES_DAY_LORE)
                .build(), 2 * DEFAULT_WEIGHT);

        add(ItemTypes.BOW);

        // Not many arrows
        add(ItemTypes.ARROW, DEFAULT_WEIGHT / 2);

        // Many tipped arrows (:
        add(ItemTypes.TIPPED_ARROW, DEFAULT_WEIGHT * 6);

        // Potions in the potion gift list
        add(ItemTypes.POTION, DEFAULT_WEIGHT * 4);
    }

}
