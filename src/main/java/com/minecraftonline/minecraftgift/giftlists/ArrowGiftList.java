package com.minecraftonline.minecraftgift.giftlists;

import com.minecraftonline.minecraftgift.MinecraftGift;
import com.minecraftonline.minecraftgift.WeightedItemDeterminer;
import org.slf4j.Logger;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class ArrowGiftList extends WeightedItemDeterminer {

    private static final String GIFT_LIST_NAME = "Arrow";

    private static final Text handoutText = Text.of(
            TextColors.GREEN, "Arrow handout for testing"
    );

    @Override
    public String giftListName() {
        return GIFT_LIST_NAME;
    }

    public Text giftHandoutText() {
        return handoutText;
    }

    public ArrowGiftList() {
        final Logger logger = MinecraftGift.getLogger();
        log.info("Initializing the " + giftListName() + " gift list");

       add(ItemTypes.ARROW);
       // And tipped arrows...
       add(ItemTypes.TIPPED_ARROW);
    }

}
