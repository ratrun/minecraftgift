package com.minecraftonline.minecraftgift.giftlists;

import com.minecraftonline.minecraftgift.GiftTextUtils;
import com.minecraftonline.minecraftgift.WeightedItemDeterminer;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.*;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PrideGiftList extends WeightedItemDeterminer {

    private static final String GIFT_LIST_NAME = "Pride";

    private static final int SPECIAL_MAP_ID = 6183;

    private Map<DyeColor, TextColor> prideTextColorMap = new HashMap<>();

    private Text handoutText = Text.of(TextColors.GREEN, "Hourly ",
            TextColors.DARK_RED, "r",
            TextColors.GOLD, "a",
            TextColors.YELLOW, "i",
            TextColors.GREEN, "n",
            TextColors.BLUE, "b",
            TextColors.DARK_BLUE, "o",
            TextColors.DARK_PURPLE, "w",
            TextColors.GREEN, " gift handout! Only at MinecraftOnline!"
    );

    @Override

    public String giftListName() { return GIFT_LIST_NAME; }

    public Text giftHandoutText() {
        return this.handoutText;
    }

    // Fix this after the consolidation
    public int getSpecialMapId() { return SPECIAL_MAP_ID; }

    public PrideGiftList() {
        log.info("Initializing the " + giftListName() + " gift list");

        final int PRIDE_BLOCK_SPECIAL_WEIGHT = DEFAULT_WEIGHT;
        final int PRIDE_BLOCK_DEFAULT_WEIGHT = DEFAULT_WEIGHT * 8;

        prideTextColorMap.put(DyeColors.RED,TextColors.RED);
        prideTextColorMap.put(DyeColors.ORANGE,TextColors.GOLD);
        prideTextColorMap.put(DyeColors.YELLOW,TextColors.YELLOW);
        prideTextColorMap.put(DyeColors.LIME,TextColors.GREEN);
        prideTextColorMap.put(DyeColors.BLUE,TextColors.BLUE);
        prideTextColorMap.put(DyeColors.PURPLE,TextColors.DARK_PURPLE);
        Text.Builder builder = Text.builder();
        builder.append(Text.of(TextColors.GREEN, "Hourly "));

        add(ItemTypes.GOLD_ORE);
        add(ItemTypes.IRON_ORE);
        add(ItemTypes.COAL_ORE);
        add(ItemTypes.LAPIS_ORE);
        add(ItemTypes.GLASS);
        add(ItemTypes.SANDSTONE);
        add(ItemTypes.GOLDEN_RAIL);

        DyeColor[] prideColors = new DyeColor[] {
                        DyeColors.RED,
                        DyeColors.ORANGE,
                        DyeColors.YELLOW,
                        DyeColors.LIME,
                        DyeColors.BLUE,
                        DyeColors.PURPLE
                };

        int year = Calendar.getInstance().get(Calendar.YEAR);
        int prideBlockNumber = 1;

        for (DyeColor color : prideColors) {
            add(ItemStack.builder()
                    .itemType(ItemTypes.WOOL)
                    .add(Keys.DYE_COLOR, color)
                    .add(Keys.DISPLAY_NAME, Text.of(prideTextColorMap.get(color), "Pride Block"))
                    .add(Keys.ITEM_LORE, Arrays.asList(
                            Text.of(TextColors.GRAY, "Pride Month ", year),
                            Text.of(TextColors.GRAY, GiftTextUtils.capitaliseFirstLetter(color.getName()), " Pride Block (", prideBlockNumber, " of ", prideColors.length, ")")
                    ))
                    .build(), PRIDE_BLOCK_DEFAULT_WEIGHT);
            prideBlockNumber++;
        }

        add(ItemStack.builder()
                .itemType(ItemTypes.WOOL)
                .add(Keys.DYE_COLOR, DyeColors.PINK)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.LIGHT_PURPLE, "Pride Block"))
                .add(Keys.ITEM_LORE, Arrays.asList(
                        Text.of(TextColors.GRAY, "Pride Month ", year),
                        Text.of(TextColors.GRAY, "Pink Pride Block (7 of ", prideColors.length, ")")
                ))
                .build(), PRIDE_BLOCK_SPECIAL_WEIGHT);


        add(ItemStack.builder()
                .itemType(ItemTypes.DOUBLE_PLANT)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.RED, "Pr", TextColors.GOLD, "id", TextColors.YELLOW, "e ", TextColors.GREEN, "Fl", TextColors.BLUE, "ow", TextColors.DARK_PURPLE, "er"))
                .add(Keys.ITEM_LORE, Arrays.asList(
                        Text.of(TextColors.GRAY, "Pride Month ", year))
                )
                .add(Keys.DOUBLE_PLANT_TYPE, DoublePlantTypes.SYRINGA)
                .build(), DEFAULT_WEIGHT * 6);


        for (DyeColor color : Sponge.getRegistry().getAllOf(DyeColor.class)) {
            add(ItemStack.builder().itemType(ItemTypes.STAINED_GLASS).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.STAINED_GLASS_PANE).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.CARPET).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.DYE).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.STAINED_HARDENED_CLAY).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.CONCRETE_POWDER).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.CONCRETE).add(Keys.DYE_COLOR, color).build());
        }

        add(ItemTypes.YELLOW_FLOWER);

        for (PlantType plantType : Sponge.getRegistry().getAllOf(PlantType.class)) {
            add(ItemStack.builder().itemType(ItemTypes.RED_FLOWER).add(Keys.PLANT_TYPE, plantType).build());
        }

        for (DoublePlantType doublePlantType : Sponge.getRegistry().getAllOf(DoublePlantType.class)) {
            // exclude grass and fern double plant items
            if (doublePlantType == DoublePlantTypes.FERN || doublePlantType == DoublePlantTypes.GRASS) continue;
            add(ItemStack.builder().itemType(ItemTypes.DOUBLE_PLANT).add(Keys.DOUBLE_PLANT_TYPE, doublePlantType).build());
        }

        add(ItemTypes.BROWN_MUSHROOM);
        add(ItemTypes.RED_MUSHROOM);
        add(ItemTypes.GOLD_BLOCK);
        add(ItemTypes.IRON_BLOCK);
        add(ItemTypes.MOSSY_COBBLESTONE);
        add(ItemTypes.OBSIDIAN);
        add(ItemTypes.TORCH);
        add(ItemTypes.MOB_SPAWNER);
        add(ItemTypes.CHEST);
        add(ItemTypes.DIAMOND_ORE);
        add(ItemTypes.DIAMOND_BLOCK);
        add(ItemTypes.CRAFTING_TABLE);
        add(ItemTypes.RAIL);
        add(ItemTypes.LEVER);
        add(ItemTypes.STONE_PRESSURE_PLATE);
        add(ItemTypes.WOODEN_PRESSURE_PLATE);
        add(ItemTypes.REDSTONE_ORE);
        add(ItemTypes.REDSTONE_TORCH);
        add(ItemTypes.ICE);
        add(ItemTypes.SNOW);
        add(ItemTypes.CACTUS);
        add(ItemTypes.CLAY);
        add(ItemTypes.JUKEBOX);
        add(ItemTypes.BROWN_MUSHROOM_BLOCK);
        add(ItemTypes.RED_MUSHROOM_BLOCK);
        add(ItemTypes.GLASS_PANE);
        add(ItemTypes.WATERLILY);
        add(ItemTypes.ENCHANTING_TABLE);
        add(ItemTypes.STONE);
        add(ItemTypes.GRASS);
        add(ItemTypes.DIRT);
        add(ItemTypes.SAPLING);
        add(ItemTypes.GOLD_ORE);
        add(ItemTypes.IRON_ORE);
        add(ItemTypes.COAL_ORE);
        add(ItemTypes.SPONGE);
        add(ItemTypes.GLASS);
        add(ItemTypes.LAPIS_ORE);
        add(ItemTypes.LAPIS_BLOCK);
        add(ItemTypes.SANDSTONE);
        add(ItemTypes.NOTEBLOCK);
        add(ItemTypes.GOLDEN_RAIL);
        add(ItemTypes.DETECTOR_RAIL);
        add(ItemTypes.STICKY_PISTON);
        add(ItemTypes.WEB);
        add(ItemTypes.PISTON);
        add(ItemTypes.WOOL);
        add(ItemTypes.YELLOW_FLOWER);
        add(ItemTypes.RED_FLOWER);
        add(ItemTypes.BROWN_MUSHROOM);
        add(ItemTypes.RED_MUSHROOM);
        add(ItemTypes.GOLD_BLOCK);
        add(ItemTypes.IRON_BLOCK);
        add(ItemTypes.BRICK_BLOCK);
        add(ItemTypes.TNT);
        add(ItemTypes.BOOKSHELF);
        add(ItemTypes.MOSSY_COBBLESTONE);
        add(ItemTypes.OBSIDIAN);
        add(ItemTypes.TORCH);
        add(ItemTypes.MOB_SPAWNER);
        add(ItemTypes.CHEST);
        add(ItemTypes.DIAMOND_ORE);
        add(ItemTypes.DIAMOND_BLOCK);
        add(ItemTypes.CRAFTING_TABLE);
        add(ItemTypes.FURNACE);
        add(ItemTypes.LADDER);
        add(ItemTypes.RAIL);
        add(ItemTypes.LEVER);
        add(ItemTypes.STONE_PRESSURE_PLATE);
        add(ItemTypes.WOODEN_PRESSURE_PLATE);
        add(ItemTypes.REDSTONE_ORE);
        add(ItemTypes.REDSTONE_TORCH);
        add(ItemTypes.STONE_BUTTON);
        add(ItemTypes.ICE);
        add(ItemTypes.SNOW);
        add(ItemTypes.CACTUS);
        add(ItemTypes.CLAY);
        add(ItemTypes.JUKEBOX);
        add(ItemTypes.FENCE);
        add(ItemTypes.PUMPKIN);
        add(ItemTypes.NETHERRACK);
        add(ItemTypes.SOUL_SAND);
        add(ItemTypes.GLOWSTONE);
        add(ItemTypes.LIT_PUMPKIN);
        add(ItemTypes.STAINED_GLASS);
        add(ItemTypes.TRAPDOOR);
        add(ItemTypes.STONEBRICK);
        add(ItemTypes.BROWN_MUSHROOM_BLOCK);
        add(ItemTypes.RED_MUSHROOM_BLOCK);
        add(ItemTypes.IRON_BARS);
        add(ItemTypes.GLASS_PANE);
        add(ItemTypes.MELON_BLOCK);
        add(ItemTypes.VINE);
        add(ItemTypes.FENCE_GATE);
        add(ItemTypes.MYCELIUM);
        add(ItemTypes.WATERLILY);
        add(ItemTypes.NETHER_BRICK);
        add(ItemTypes.NETHER_BRICK_FENCE);
        add(ItemTypes.ENCHANTING_TABLE);
        add(ItemTypes.END_STONE);
        add(ItemTypes.REDSTONE_LAMP);
        add(ItemTypes.WOODEN_SLAB);
        add(ItemTypes.SANDSTONE_STAIRS);
        add(ItemTypes.EMERALD_ORE);
        add(ItemTypes.ENDER_CHEST);
        add(ItemTypes.TRIPWIRE_HOOK);
        add(ItemTypes.EMERALD_BLOCK);
        add(ItemTypes.SPRUCE_STAIRS);
        add(ItemTypes.BIRCH_STAIRS);
        add(ItemTypes.JUNGLE_STAIRS);
        add(ItemTypes.COBBLESTONE_WALL);
        add(ItemTypes.WOODEN_BUTTON);
        add(ItemTypes.ANVIL);
        add(ItemTypes.TRAPPED_CHEST);
        add(ItemTypes.LIGHT_WEIGHTED_PRESSURE_PLATE);
        add(ItemTypes.HEAVY_WEIGHTED_PRESSURE_PLATE);
        add(ItemTypes.DAYLIGHT_DETECTOR);
        add(ItemTypes.REDSTONE_BLOCK);
        add(ItemTypes.QUARTZ_ORE);
        add(ItemTypes.HOPPER);
        add(ItemTypes.QUARTZ_BLOCK);
        add(ItemTypes.QUARTZ_STAIRS);
        add(ItemTypes.ACTIVATOR_RAIL);
        add(ItemTypes.DROPPER);
        add(ItemTypes.STAINED_HARDENED_CLAY);
        add(ItemTypes.STAINED_GLASS_PANE);
        add(ItemTypes.ACACIA_STAIRS);
        add(ItemTypes.DARK_OAK_STAIRS);
        add(ItemTypes.SLIME);
        add(ItemTypes.IRON_TRAPDOOR);
        add(ItemTypes.PRISMARINE);
        add(ItemTypes.SEA_LANTERN);
        add(ItemTypes.HAY_BLOCK);
        add(ItemTypes.CARPET);
        add(ItemTypes.HARDENED_CLAY);
        add(ItemTypes.COAL_BLOCK);
        add(ItemTypes.PACKED_ICE);
        add(ItemTypes.DOUBLE_PLANT);
        add(ItemTypes.RED_SANDSTONE);
        add(ItemTypes.RED_SANDSTONE_STAIRS);
        add(ItemTypes.STONE_SLAB2);
        add(ItemTypes.SPRUCE_FENCE_GATE);
        add(ItemTypes.BIRCH_FENCE_GATE);
        add(ItemTypes.JUNGLE_FENCE_GATE);
        add(ItemTypes.DARK_OAK_FENCE_GATE);
        add(ItemTypes.ACACIA_FENCE_GATE);
        add(ItemTypes.SPRUCE_FENCE);
        add(ItemTypes.BIRCH_FENCE);
        add(ItemTypes.JUNGLE_FENCE);
        add(ItemTypes.DARK_OAK_FENCE);
        add(ItemTypes.ACACIA_FENCE);
        add(ItemTypes.MAGMA);
        add(ItemTypes.NETHER_WART_BLOCK);
        add(ItemTypes.RED_NETHER_BRICK);
        add(ItemTypes.BONE_BLOCK);
        add(ItemTypes.WHITE_GLAZED_TERRACOTTA);
        add(ItemTypes.ORANGE_GLAZED_TERRACOTTA);
        add(ItemTypes.MAGENTA_GLAZED_TERRACOTTA);
        add(ItemTypes.LIGHT_BLUE_GLAZED_TERRACOTTA);
        add(ItemTypes.YELLOW_GLAZED_TERRACOTTA);
        add(ItemTypes.LIME_GLAZED_TERRACOTTA);
        add(ItemTypes.PINK_GLAZED_TERRACOTTA);
        add(ItemTypes.GRAY_GLAZED_TERRACOTTA);
        add(ItemTypes.SILVER_GLAZED_TERRACOTTA);
        add(ItemTypes.CYAN_GLAZED_TERRACOTTA);
        add(ItemTypes.PURPLE_GLAZED_TERRACOTTA);
        add(ItemTypes.BLUE_GLAZED_TERRACOTTA);
        add(ItemTypes.BROWN_GLAZED_TERRACOTTA);
        add(ItemTypes.GREEN_GLAZED_TERRACOTTA);
        add(ItemTypes.RED_GLAZED_TERRACOTTA);
        add(ItemTypes.BLACK_GLAZED_TERRACOTTA);
        add(ItemTypes.IRON_SHOVEL);
        add(ItemTypes.IRON_PICKAXE);
        add(ItemTypes.IRON_AXE);
        add(ItemTypes.APPLE);
        add(ItemTypes.BOW);
        add(ItemTypes.ARROW);
        add(ItemTypes.COAL);
        add(ItemTypes.DIAMOND);
        add(ItemTypes.IRON_INGOT);
        add(ItemTypes.GOLD_INGOT);
        add(ItemTypes.IRON_SWORD);
        add(ItemTypes.WOODEN_SWORD);
        add(ItemTypes.WOODEN_SHOVEL);
        add(ItemTypes.WOODEN_PICKAXE);
        add(ItemTypes.WOODEN_AXE);
        add(ItemTypes.STONE_SWORD);
        add(ItemTypes.STONE_SHOVEL);
        add(ItemTypes.STONE_PICKAXE);
        add(ItemTypes.STONE_AXE);
        add(ItemTypes.DIAMOND_SWORD);
        add(ItemTypes.DIAMOND_SHOVEL);
        add(ItemTypes.DIAMOND_PICKAXE);
        add(ItemTypes.DIAMOND_AXE);
        add(ItemTypes.STICK);
        add(ItemTypes.BOWL);
        add(ItemTypes.MUSHROOM_STEW);
        add(ItemTypes.GOLDEN_SWORD);
        add(ItemTypes.GOLDEN_SHOVEL);
        add(ItemTypes.GOLDEN_PICKAXE);
        add(ItemTypes.GOLDEN_AXE);
        add(ItemTypes.STRING);
        add(ItemTypes.FEATHER);
        add(ItemTypes.GUNPOWDER);
        add(ItemTypes.WOODEN_HOE);
        add(ItemTypes.STONE_HOE);
        add(ItemTypes.IRON_HOE);
        add(ItemTypes.DIAMOND_HOE);
        add(ItemTypes.GOLDEN_HOE);
        add(ItemTypes.WHEAT_SEEDS);
        add(ItemTypes.WHEAT);
        add(ItemTypes.BREAD);
        add(ItemTypes.LEATHER_HELMET);
        add(ItemTypes.LEATHER_CHESTPLATE);
        add(ItemTypes.LEATHER_LEGGINGS);
        add(ItemTypes.LEATHER_BOOTS);
        add(ItemTypes.CHAINMAIL_HELMET);
        add(ItemTypes.CHAINMAIL_CHESTPLATE);
        add(ItemTypes.CHAINMAIL_LEGGINGS);
        add(ItemTypes.CHAINMAIL_BOOTS);
        add(ItemTypes.IRON_HELMET);
        add(ItemTypes.IRON_CHESTPLATE);
        add(ItemTypes.IRON_LEGGINGS);
        add(ItemTypes.IRON_BOOTS);
        add(ItemTypes.DIAMOND_HELMET);
        add(ItemTypes.DIAMOND_CHESTPLATE);
        add(ItemTypes.DIAMOND_LEGGINGS);
        add(ItemTypes.DIAMOND_BOOTS);
        add(ItemTypes.GOLDEN_HELMET);
        add(ItemTypes.GOLDEN_CHESTPLATE);
        add(ItemTypes.GOLDEN_LEGGINGS);
        add(ItemTypes.GOLDEN_BOOTS);
        add(ItemTypes.FLINT);
        add(ItemTypes.PORKCHOP);
        add(ItemTypes.COOKED_PORKCHOP);
        add(ItemTypes.PAINTING);
        add(ItemTypes.SIGN);
        add(ItemTypes.WOODEN_DOOR);
        add(ItemTypes.BUCKET);
        add(ItemTypes.WATER_BUCKET);
        add(ItemTypes.MINECART);
        add(ItemTypes.SADDLE);
        add(ItemTypes.IRON_DOOR);
        add(ItemTypes.REDSTONE);
        add(ItemTypes.SNOWBALL);
        add(ItemTypes.BOAT);
        add(ItemTypes.LEATHER);
        add(ItemTypes.MILK_BUCKET);
        add(ItemTypes.BRICK);
        add(ItemTypes.CLAY_BALL);
        add(ItemTypes.REEDS);
        add(ItemTypes.PAPER);
        add(ItemTypes.BOOK);
        add(ItemTypes.SLIME_BALL);
        add(ItemTypes.CHEST_MINECART);
        add(ItemTypes.FURNACE_MINECART);
        add(ItemTypes.EGG);
        add(ItemTypes.COMPASS);
        add(ItemTypes.FISHING_ROD);
        add(ItemTypes.CLOCK);
        add(ItemTypes.GLOWSTONE_DUST);
        add(ItemTypes.FISH);
        add(ItemTypes.COOKED_FISH);
        add(ItemTypes.DYE);
        add(ItemTypes.BONE);
        add(ItemTypes.SUGAR);
        add(ItemTypes.CAKE);
        add(ItemTypes.BED);
        add(ItemTypes.REPEATER);
        add(ItemTypes.COOKIE);
        add(ItemTypes.FILLED_MAP);
        add(ItemTypes.SHEARS);
        add(ItemTypes.MELON);
        add(ItemTypes.PUMPKIN_SEEDS);
        add(ItemTypes.MELON_SEEDS);
        add(ItemTypes.BEEF);
        add(ItemTypes.COOKED_BEEF);
        add(ItemTypes.CHICKEN);
        add(ItemTypes.COOKED_CHICKEN);
        add(ItemTypes.ROTTEN_FLESH);
        add(ItemTypes.ENDER_PEARL);
        add(ItemTypes.BLAZE_ROD);
        add(ItemTypes.GHAST_TEAR);
        add(ItemTypes.GOLD_NUGGET);
        add(ItemTypes.NETHER_WART);
        // Make potions weightier for they are more plentiful in type
        add(ItemTypes.POTION, DEFAULT_WEIGHT * 5);
        add(ItemTypes.GLASS_BOTTLE);
        add(ItemTypes.SPIDER_EYE);
        add(ItemTypes.FERMENTED_SPIDER_EYE);
        add(ItemTypes.BLAZE_POWDER);
        add(ItemTypes.MAGMA_CREAM);
        add(ItemTypes.BREWING_STAND);
        add(ItemTypes.CAULDRON);
        add(ItemTypes.ENDER_EYE);
        add(ItemTypes.SPECKLED_MELON);
        add(ItemTypes.WRITABLE_BOOK);
        add(ItemTypes.EMERALD);
        add(ItemTypes.ITEM_FRAME);
        add(ItemTypes.FLOWER_POT);
        add(ItemTypes.CARROT);
        add(ItemTypes.POTATO);
        add(ItemTypes.BAKED_POTATO);
        add(ItemTypes.POISONOUS_POTATO);
        add(ItemTypes.MAP);
        add(ItemTypes.GOLDEN_CARROT);
        add(ItemTypes.SKULL);
        add(ItemTypes.CARROT_ON_A_STICK);
        add(ItemTypes.PUMPKIN_PIE);
        add(ItemTypes.FIREWORKS);
        add(ItemTypes.FIREWORK_CHARGE);
        add(ItemTypes.COMPARATOR);
        add(ItemTypes.NETHERBRICK);
        add(ItemTypes.QUARTZ);
        add(ItemTypes.TNT_MINECART);
        add(ItemTypes.HOPPER_MINECART);
        add(ItemTypes.PRISMARINE_SHARD);
        add(ItemTypes.PRISMARINE_CRYSTALS);
        add(ItemTypes.RABBIT);
        add(ItemTypes.COOKED_RABBIT);
        add(ItemTypes.RABBIT_STEW);
        add(ItemTypes.RABBIT_FOOT);
        add(ItemTypes.RABBIT_HIDE);
        add(ItemTypes.ARMOR_STAND);
        add(ItemTypes.IRON_HORSE_ARMOR);
        add(ItemTypes.GOLDEN_HORSE_ARMOR);
        add(ItemTypes.DIAMOND_HORSE_ARMOR);
        add(ItemTypes.LEAD);
        add(ItemTypes.NAME_TAG);
        add(ItemTypes.MUTTON);
        add(ItemTypes.COOKED_MUTTON);
        add(ItemTypes.BIRCH_FENCE);
        add(ItemTypes.JUNGLE_FENCE);
        add(ItemTypes.DARK_OAK_FENCE);
        add(ItemTypes.ACACIA_FENCE);
        add(ItemTypes.SPRUCE_DOOR);
        add(ItemTypes.BIRCH_DOOR);
        add(ItemTypes.JUNGLE_DOOR);
        add(ItemTypes.ACACIA_DOOR);
        add(ItemTypes.DARK_OAK_DOOR);
        add(ItemTypes.BEETROOT);
        add(ItemTypes.BEETROOT_SEEDS);
        add(ItemTypes.BEETROOT_SOUP);
        add(ItemTypes.SHIELD);
        add(ItemTypes.SPRUCE_BOAT);
        add(ItemTypes.BIRCH_BOAT);
        add(ItemTypes.JUNGLE_BOAT);
        add(ItemTypes.ACACIA_BOAT);
        add(ItemTypes.DARK_OAK_BOAT);
        add(ItemTypes.IRON_NUGGET);
        add(ItemTypes.RECORD_13);
        add(ItemTypes.RECORD_CAT);
        add(ItemTypes.RECORD_BLOCKS);
        add(ItemTypes.RECORD_CHIRP);
        add(ItemTypes.RECORD_FAR);
        add(ItemTypes.RECORD_MALL);
        add(ItemTypes.RECORD_MELLOHI);
        add(ItemTypes.RECORD_STAL);
        add(ItemTypes.RECORD_STRAD);
        add(ItemTypes.RECORD_WARD);
        add(ItemTypes.RECORD_11);
        add(ItemTypes.RECORD_WAIT);
        // Adding special arrows!
        add(ItemTypes.TIPPED_ARROW);
    }
}
