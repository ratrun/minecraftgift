package com.minecraftonline.minecraftgift.giftlists;

import com.minecraftonline.minecraftgift.MinecraftGift;
import com.minecraftonline.minecraftgift.WeightedItemDeterminer;
import org.slf4j.Logger;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.item.FireworkEffect;
import org.spongepowered.api.item.FireworkShapes;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Color;

import java.util.Arrays;
import java.util.Calendar;

import static org.spongepowered.api.item.ItemTypes.RED_FLOWER;

public class BirthdayGiftList extends WeightedItemDeterminer {

    private static final String GIFT_LIST_NAME = "Birthday";


    // MCO Logo Pixel Art thing (Thanks Anna! (: )
    private static final int MAP_ID = 2082;

    @Override
    public Text giftHandoutText() {
        int birthday = Calendar.getInstance().get(Calendar.YEAR) - 2010;
        return Text.of(TextColors.GREEN, "Hourly ", birthday, "th Birthday random gift handout! Only at MinecraftOnline!");
    }

    public String giftListName() {
        return GIFT_LIST_NAME;
    }

    public int SPECIAL_MAP_ID() { return MAP_ID; }

    public BirthdayGiftList() {
        final Logger logger = MinecraftGift.getLogger();
        log.info("Initializing the " + giftListName() + " gift list");

        int birthday = Calendar.getInstance().get(Calendar.YEAR) - 2010;

        // Ted doesn't know which.
        final DyeColor BIRTHDAY_GIFT_DYE_COLOR = DyeColors.CYAN;
        final Color BIRTHDAY_GIFT_COLOR = Color.CYAN;
        // final DyeColor BIRTHDAY_GIFT_DYE_COLOR = DyeColors.LIGHT_BLUE;

        final int MCO_LOGO_MAP_ID = 2082;

        add(ItemStack.builder()
                .itemType(ItemTypes.DIAMOND_PICKAXE)
                .add(Keys.ITEM_ENCHANTMENTS, Arrays.asList(Enchantment.builder().type(EnchantmentTypes.UNBREAKING).level(3).build()))
                .build(),
        6 * DEFAULT_WEIGHT);

        add(ItemStack.builder()
                .itemType(ItemTypes.DIAMOND_AXE)
                .add(Keys.ITEM_ENCHANTMENTS, Arrays.asList(Enchantment.builder().type(EnchantmentTypes.UNBREAKING).level(3).build()))
                .build()
        ,6 * DEFAULT_WEIGHT);

        add(ItemTypes.DIAMOND_BLOCK, 2 * DEFAULT_WEIGHT );

        add(ItemTypes.DIAMOND);

        add(ItemTypes.DIAMOND_HORSE_ARMOR, DEFAULT_WEIGHT / 2);

        add(ItemStack.builder()
                .itemType(ItemTypes.WOOL)
                .add(Keys.DYE_COLOR, BIRTHDAY_GIFT_DYE_COLOR)
                .build()
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.STAINED_HARDENED_CLAY)
                .add(Keys.DYE_COLOR, BIRTHDAY_GIFT_DYE_COLOR)
                .build()
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.STAINED_GLASS)
                .add(Keys.DYE_COLOR, BIRTHDAY_GIFT_DYE_COLOR)
                .build()
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.STAINED_GLASS_PANE)
                .add(Keys.DYE_COLOR, BIRTHDAY_GIFT_DYE_COLOR)
                .build()
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.CONCRETE)
                .add(Keys.DYE_COLOR, BIRTHDAY_GIFT_DYE_COLOR)
                .build()
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.CONCRETE_POWDER)
                .add(Keys.DYE_COLOR, BIRTHDAY_GIFT_DYE_COLOR)
                .build()
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.BANNER)
                .add(Keys.DYE_COLOR, BIRTHDAY_GIFT_DYE_COLOR)
                .build()
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.BED)
                .add(Keys.DYE_COLOR, BIRTHDAY_GIFT_DYE_COLOR)
                .build()
        );

        add(ItemTypes.CYAN_GLAZED_TERRACOTTA);

        add(ItemStack.builder()
                .itemType(ItemTypes.DYE)
                .add(Keys.DYE_COLOR, BIRTHDAY_GIFT_DYE_COLOR)
                .build()
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.COOKIE)
                .quantity(birthday)
                .build()
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.CARPET)
                .add(Keys.DYE_COLOR, BIRTHDAY_GIFT_DYE_COLOR)
                .build()
        );

        // Because this is how you create the BLUE FLOWER CALLED THE BLUE ORCHID
        add(ItemStack.builder().fromContainer(ItemStack.of(RED_FLOWER)
                        .toContainer()
                        .set(DataQuery.of("UnsafeDamage"), 1)).build()
        );

        // Very rare
        add(ItemTypes.CYAN_SHULKER_BOX, 1);

        // Big thank to Tyh
        add(ItemStack.builder().fromContainer(ItemStack.of(ItemTypes.FILLED_MAP)
                .toContainer()
                .set(DataQuery.of("UnsafeDamage"), MCO_LOGO_MAP_ID)).build()
                , 6 * DEFAULT_WEIGHT
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.CAKE)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "Birthday Cake"))
                .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "For MinecraftOnline's ", birthday, "th Birthday")))
                .build()
        );

        // https://www.digminecraft.com/lists/fireworks_color_list_pc.php
        // $client "give $player fireworks 8 0 {Fireworks:{Flight:1,Explosions:[{Type:1,Flicker:0,Trail:0,Colors:[6719955],FadeColors:[15790320]}]}}"

        add(ItemStack.builder()
                .itemType(ItemTypes.FIREWORKS)
                .add(Keys.FIREWORK_EFFECTS, Arrays.asList(FireworkEffect.builder()
                        .shape(FireworkShapes.STAR)
                        .color(BIRTHDAY_GIFT_COLOR)
                        .fades(Color.WHITE)
                        .build()))
                .add(Keys.FIREWORK_FLIGHT_MODIFIER, 1)
                .quantity(birthday)
                .build()
        );

        // Add some sort of custom potions here?

        add(ItemTypes.GLOWSTONE);

        add(ItemTypes.RECORD_WARD);
        add(ItemTypes.RECORD_WAIT);
        add(ItemTypes.RECORD_STRAD);
        add(ItemTypes.RECORD_STAL);
        add(ItemTypes.RECORD_MELLOHI);
        add(ItemTypes.RECORD_MALL);
        add(ItemTypes.RECORD_FAR);
        add(ItemTypes.RECORD_CHIRP);
        add(ItemTypes.RECORD_CAT);
        add(ItemTypes.RECORD_BLOCKS);
        add(ItemTypes.RECORD_13);
        add(ItemTypes.RECORD_11);

        add(ItemTypes.NOTEBLOCK);
        add(ItemTypes.JUKEBOX);

        add(ItemTypes.DIAMOND_SWORD);
        add(ItemTypes.DIAMOND_SHOVEL);
        add(ItemTypes.DIAMOND_HOE);

        add(ItemTypes.DIAMOND_HELMET);
        add(ItemTypes.DIAMOND_CHESTPLATE);
        add(ItemTypes.DIAMOND_LEGGINGS);
        add(ItemTypes.DIAMOND_BOOTS);

        // Adding special arrows!
        add(ItemTypes.TIPPED_ARROW);
    }

}
