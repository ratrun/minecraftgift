package com.minecraftonline.minecraftgift.giftlists;

import com.minecraftonline.minecraftgift.WeightedItemDeterminer;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.data.type.TreeTypes;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Color;

import java.util.Arrays;
import java.util.Calendar;

public class ChristmasGiftList extends WeightedItemDeterminer {
    private static final String GIFT_LIST_NAME = "Christmas";

    @Override

    public String giftListName() { return GIFT_LIST_NAME; }

    // A fun override
    int SPECIAL_MAP_ID = 8639;
    public int specialMapID() { return SPECIAL_MAP_ID; };

    public Text giftHandoutText() {
        return Text.of(TextColors.GREEN, "Hourly Christmas random gift handout! Only at MinecraftOnline!");
    }

    /* public int SPECIAL_MAP_ID() { return 8639; }; */

    public ChristmasGiftList() {
        log.info("Initializing the " + giftListName() + " gift list");

        int year = Calendar.getInstance().get(Calendar.YEAR);

        add(ItemStack.builder()
                .itemType(ItemTypes.SAPLING)
                .add(Keys.TREE_TYPE, TreeTypes.SPRUCE)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.GREEN, "Christmas Tree"))
                .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "Christmas ", year)))
                .build(), DEFAULT_WEIGHT * 8);

        // Eggnog would be here, but it's actually in the ChristmasPotionList!
        add(ItemTypes.POTION, 4 * DEFAULT_WEIGHT);

        add(ItemStack.builder()
                .itemType(ItemTypes.LOG)
                .add(Keys.TREE_TYPE, TreeTypes.SPRUCE)
                .build());

        add(ItemStack.builder()
                .itemType(ItemTypes.LEAVES)
                .add(Keys.TREE_TYPE, TreeTypes.SPRUCE)
                .build());

        add(ItemTypes.NOTEBLOCK);

        DyeColor[] christmasColors = new DyeColor[] {
                DyeColors.RED,
                DyeColors.GREEN,
        };

        ItemType[] dyedItemTypes = new ItemType[] {
                ItemTypes.DYE,
                ItemTypes.STAINED_GLASS,
                ItemTypes.STAINED_GLASS_PANE,
                ItemTypes.CONCRETE_POWDER,
                ItemTypes.CONCRETE,
                ItemTypes.STAINED_HARDENED_CLAY,
                ItemTypes.WOOL,
                ItemTypes.CARPET,
                // Ted feels like adding ugly festively-colored armor
        };



        // It appears as though leather armor requires special treatment.
        ItemType[] leatherArmor = new ItemType[] {
                ItemTypes.LEATHER_HELMET,
                ItemTypes.LEATHER_CHESTPLATE,
                ItemTypes.LEATHER_LEGGINGS,
                ItemTypes.LEATHER_BOOTS
        };

        Color[] leatherColor = new Color[] {
                Color.GREEN,
                Color.RED
        };

        for (Color c : leatherColor) {
            for (ItemType i: leatherArmor)
            {
                add (ItemStack.builder()
                        .itemType(i)
                        .add(Keys.COLOR, c)
                        .build());
            }
        }


        for (DyeColor c : christmasColors) {
            for (ItemType i : dyedItemTypes) {
                add(ItemStack.builder()
                    .itemType(i)
                    .add(Keys.DYE_COLOR, c)
                    .build());
            }
        }

        add(ItemTypes.DIAMOND_SWORD);
        add(ItemTypes.DIAMOND_SHOVEL);
        add(ItemTypes.DIAMOND_PICKAXE);
        add(ItemTypes.DIAMOND_AXE);
        add(ItemTypes.DIAMOND_HOE);
        add(ItemTypes.DIAMOND_HELMET);
        add(ItemTypes.DIAMOND_CHESTPLATE);
        add(ItemTypes.DIAMOND_LEGGINGS);
        add(ItemTypes.DIAMOND_BOOTS);

        add(ItemTypes.DIAMOND_BLOCK);
        add(ItemTypes.BRICK_BLOCK);

        add(ItemTypes.BRICK);

        add(ItemTypes.MILK_BUCKET, 3 * DEFAULT_WEIGHT);
        add(ItemStack.builder()
                .itemType(ItemTypes.SNOWBALL)
                .quantity(16)
                .build(), 4 * DEFAULT_WEIGHT);


        add(ItemTypes.COOKED_PORKCHOP);
        add(ItemTypes.COOKED_BEEF);
        add(ItemTypes.STICK);
        add(ItemTypes.MUSHROOM_STEW);
        add(ItemTypes.COAL);
        add(ItemTypes.COAL_BLOCK);
        add(ItemTypes.SNOW_LAYER, 2 * DEFAULT_WEIGHT);
        add(ItemTypes.ICE, 2 * DEFAULT_WEIGHT);
        add(ItemTypes.SNOW, 4 * DEFAULT_WEIGHT);
        add(ItemTypes.PACKED_ICE);
        add(ItemTypes.CHEST);
        add(ItemTypes.TRAPPED_CHEST);
        add(ItemTypes.CAKE);


        add(ItemTypes.CHEST_MINECART);

        add(ItemTypes.GOLDEN_APPLE);

        add(ItemTypes.COOKIE, 3 * DEFAULT_WEIGHT);

        add(ItemTypes.COOKED_FISH);

        add(ItemTypes.COOKED_CHICKEN);

        add(ItemTypes.COOKED_MUTTON);

        add(ItemTypes.COOKED_RABBIT, 2 * DEFAULT_WEIGHT);

        // Yay, I figured this out!
        add(ItemTypes.FILLED_MAP, 3 * DEFAULT_WEIGHT);

        add(ItemTypes.MELON);
        add(ItemTypes.CARROT);
        add(ItemTypes.BAKED_POTATO);

        add(ItemTypes.NETHER_STAR, 2 * DEFAULT_WEIGHT);

        add(ItemTypes.PUMPKIN_PIE);

        add(ItemTypes.FIREWORKS);

        add(ItemTypes.DIAMOND_HORSE_ARMOR);

    }
}
