package com.minecraftonline.minecraftgift.giftlists;

import com.minecraftonline.minecraftgift.MinecraftGift;
import com.minecraftonline.minecraftgift.WeightedItemDeterminer;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.*;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.potion.PotionTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Optional;

public class HalloweenGiftList extends WeightedItemDeterminer {

    private static final String GIFT_LIST_NAME = "Halloween";

    // No special map at present.
    // private static final int MAP_ID = -1;

    private static final Text handoutText = Text.of(
            TextColors.GREEN, "Hourly", TextColors.GOLD, " spooky ", TextColors.GREEN, "gift handout! Only at MinecraftOnline!"
    );

    @Override
    public String giftListName() {
        return GIFT_LIST_NAME;
    }

    public Text giftHandoutText() {
        return handoutText;
    }

    // public int SPECIAL_MAP_ID() { return MAP_ID; }

    public HalloweenGiftList() {
        log.info("Initializing the " + giftListName() + " gift list");

        int year = Calendar.getInstance().get(Calendar.YEAR);

        // Because doing this is kinda fake big brain, right? (:
        DyeColor[] HalloweenColors = new DyeColor[] {
                DyeColors.BLACK,
                DyeColors.ORANGE
        };

        // Can I mis/reappropriate this from Tyh's implementation of PrideGiftList? (:
        for (DyeColor color : HalloweenColors) {
            add(ItemStack.builder().itemType(ItemTypes.STAINED_GLASS).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.STAINED_GLASS_PANE).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.CARPET).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.DYE).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.STAINED_HARDENED_CLAY).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.CONCRETE_POWDER).add(Keys.DYE_COLOR, color).build());
            add(ItemStack.builder().itemType(ItemTypes.CONCRETE).add(Keys.DYE_COLOR, color).build());
        }

        add(ItemStack.builder()
                .itemType(ItemTypes.PLANKS)
                .add(Keys.TREE_TYPE, TreeTypes.ACACIA)
                .build());

        add(ItemStack.builder()
                .itemType(ItemTypes.SAPLING)
                .add(Keys.TREE_TYPE, TreeTypes.ACACIA)
                .build());

        add(ItemStack.builder()
                .itemType(ItemTypes.SAND)
                .add(Keys.SAND_TYPE, SandTypes.RED)
                .build());

        add(ItemTypes.GOLD_ORE);

        add(ItemTypes.NOTEBLOCK);

        add(ItemStack.builder()
                .itemType(ItemTypes.RED_FLOWER)
                .add(Keys.DYE_COLOR, DyeColors.ORANGE)
                .build());

        add(ItemTypes.GOLD_BLOCK);

        add(ItemStack.builder()
                .itemType(ItemTypes.STONE_SLAB)
                .add(Keys.SLAB_TYPE, SlabTypes.NETHERBRICK)
                .build());

        add(ItemStack.builder()
                .itemType(ItemTypes.STONE_SLAB)
                .add(Keys.SLAB_TYPE, SlabTypes.QUARTZ)
                .build());

        add(ItemTypes.MOSSY_COBBLESTONE);

        add(ItemTypes.OBSIDIAN);

        add(ItemTypes.TORCH);

        add(ItemTypes.REDSTONE_TORCH);

        add(ItemTypes.MOB_SPAWNER, DEFAULT_WEIGHT * 3);

        add(ItemStack.builder()
                .itemType(ItemTypes.PUMPKIN)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "Spooky Pumpkin"))
                .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "Halloween ", year)))
                .build(), DEFAULT_WEIGHT * 5
        );

        add(ItemStack.builder()
                .itemType(ItemTypes.LIT_PUMPKIN)
                .add(Keys.DISPLAY_NAME, Text.of(TextColors.GOLD, "Spooky Pumpkin"))
                .add(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "Halloween ", year)))
                .build(), DEFAULT_WEIGHT * 3
        );

        add(ItemTypes.NETHERRACK);

        add(ItemTypes.SOUL_SAND);

        add(ItemTypes.GLOWSTONE);

        add(ItemStack.builder()
                .itemType(ItemTypes.STONEBRICK)
                .add(Keys.BRICK_TYPE, BrickTypes.MOSSY)
                .build());

        add(ItemStack.builder()
                .itemType(ItemTypes.STONEBRICK)
                .add(Keys.BRICK_TYPE, BrickTypes.MOSSY)
                .build());

        add(ItemStack.builder()
                .itemType(ItemTypes.STONEBRICK)
                .add(Keys.BRICK_TYPE, BrickTypes.CHISELED)
                .build());

        add(ItemTypes.MYCELIUM);

        add(ItemTypes.NETHER_BRICK);

        add(ItemTypes.NETHER_BRICK_FENCE);

        add(ItemTypes.NETHER_BRICK_STAIRS);

        add(ItemTypes.ENCHANTING_TABLE);

        add(ItemTypes.END_STONE);

        add(ItemStack.builder()
                .itemType(ItemTypes.WOODEN_SLAB)
                .add(Keys.TREE_TYPE, TreeTypes.ACACIA)
                .build());

        add(ItemTypes.ENDER_CHEST);

        add(ItemTypes.QUARTZ_ORE);

        add(ItemTypes.APPLE);

        add(ItemTypes.COAL);

        add(ItemTypes.GOLD_INGOT);

        add(ItemTypes.MUSHROOM_STEW);

        add(ItemTypes.GOLDEN_SWORD);

        add(ItemTypes.GOLDEN_SHOVEL);

        add(ItemTypes.GOLDEN_PICKAXE);

        add(ItemTypes.GOLDEN_AXE);

        add(ItemTypes.GOLDEN_HOE);

        add(ItemTypes.IRON_HOE);

        add(ItemTypes.GOLDEN_HELMET);

        add(ItemTypes.GOLDEN_CHESTPLATE);

        add(ItemTypes.GOLDEN_LEGGINGS);

        add(ItemTypes.GOLDEN_BOOTS);

        add(ItemTypes.GOLDEN_APPLE);

        add(ItemTypes.SLIME_BALL);

        add(ItemTypes.BONE);

        add(ItemTypes.CAKE);

        add(ItemTypes.COOKIE);

        add(ItemTypes.PUMPKIN_SEEDS);

        add(ItemTypes.ROTTEN_FLESH);

        add(ItemTypes.ENDER_PEARL);

        add(ItemTypes.BLAZE_ROD);

        add(ItemTypes.GHAST_TEAR);

        add(ItemTypes.GOLD_NUGGET);

        add(ItemTypes.NETHER_WART);

        add(ItemStack.builder()
                .itemType(ItemTypes.POTION)
                .add(Keys.POTION_TYPE, PotionTypes.MUNDANE)
                .build());

        add(ItemTypes.SPIDER_EYE);

        add(ItemTypes.FERMENTED_SPIDER_EYE);

        add(ItemTypes.BLAZE_POWDER);

        add(ItemTypes.MAGMA_CREAM);

        add(ItemTypes.BREWING_STAND);

        add(ItemTypes.CAULDRON);

        add(ItemTypes.ENDER_EYE);

        add(ItemTypes.SPECKLED_MELON);

        add(ItemTypes.GOLDEN_CARROT);

        add(ItemTypes.SKULL);

        add(ItemStack.builder()
                .itemType(ItemTypes.SKULL)
                .add(Keys.SKULL_TYPE, SkullTypes.WITHER_SKELETON)
                .build());

        add(ItemStack.builder()
                .itemType(ItemTypes.SKULL)
                .add(Keys.SKULL_TYPE, SkullTypes.ZOMBIE)
                .build());

        add(ItemStack.builder()
                .itemType(ItemTypes.SKULL)
                .add(Keys.SKULL_TYPE, SkullTypes.CREEPER)
                .build());

        add(ItemTypes.PUMPKIN_PIE, DEFAULT_WEIGHT * 3);

        add(ItemTypes.NETHER_BRICK);

        add(ItemTypes.QUARTZ);

        add(ItemTypes.GOLDEN_HORSE_ARMOR);

        add(ItemTypes.RECORD_11);

        // Vakk's book
        // Big thanky to tyh!
        Optional<ItemStack> book = MinecraftGift.getInstance().getSpookyBook().getBook();
        if (book.isPresent()) {
            // add lore for correct year
            book.get().offer(Keys.ITEM_LORE, Arrays.asList(Text.of(TextColors.GRAY, "Halloween ", year)));
            add(book.get(), DEFAULT_WEIGHT * 5);
        }
        else {
            log.warn("Not adding spooky book to halloween gift list, as it could not be retrieved.");
        }
    }

}
