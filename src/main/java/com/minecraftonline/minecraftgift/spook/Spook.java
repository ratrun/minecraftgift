package com.minecraftonline.minecraftgift.spook;

import com.flowpowered.math.vector.Vector3d;
import com.minecraftonline.minecraftgift.MinecraftGift;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.monster.EntityEnderman;
import org.slf4j.Logger;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.monster.Enderman;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Collection;
import java.util.Random;

public class Spook {
    // Tunables, as taken from the bash script
    final int DISTANCE = 4;
    // Normal type
    final int DEFAULT_TYPE = 0;

    // We're now working with blocks instead of items because
    // tyh made me realize that Enderman don't actually hold items
    // because they're weird like that.


    public static CommandResult SpawnEnderman(CommandSource source, CommandContext context) throws CommandException {

        Logger log = MinecraftGift.getLogger();
        Collection<Player> targets = context.getAll("Player");
        // I lack the brainpower to deal with this being optional.
        // Optional<Object> blockType = context.getOne("BlockType");

        // Random!
        final Random rand = new Random();
        // log.info("There are " + SPOOKY_BLOCKS.length + " Spooky Blocks");

        // I realize these probably don't need to be *here*, but my brain can't make
        // it work when above.

        // TODO: implement player-settable stuff
        BlockType[] SPOOKY_BLOCKS = new BlockType[]
        {
                BlockTypes.END_STONE,
                BlockTypes.CAKE,
                BlockTypes.STONEBRICK,
                BlockTypes.PLANKS,
                // BlockTypes.DIAMOND_BLOCK,
                BlockTypes.PUMPKIN,
                BlockTypes.LIT_PUMPKIN,
        };
        for (Player t : targets) {
            Vector3d playerPos = t.getPosition();
            Vector3d playerRotation = t.getHeadRotation();

            // Because my brain can't into the bash math
            Vector3d pos = playerPos.sub(rand.nextInt(6)-2,0,rand.nextInt(6)-2);

            // Like WHAT THE FUCK is this
            /*
                nx=$(echo "$x+($distance*s($rotation*((4*a(1))/180)))" | bc -l)
                nz=$(echo "$z-($distance*c($rotation*((4*a(1))/180)))" | bc -l)
             */

            BlockType SpookyBlock = SPOOKY_BLOCKS[rand.nextInt(SPOOKY_BLOCKS.length)];

            Enderman enderman = (Enderman) t.getWorld().createEntity(EntityTypes.ENDERMAN, pos);
            enderman.setRotation(playerRotation);
            // Ineffective
            /*
                    enderman.offer(Keys.SHOULD_DROP, false);
                    enderman.offer(Keys.CAN_DROP_AS_ITEM, false);
             */

            EntityEnderman mcEntity = (EntityEnderman) enderman;
            mcEntity.setHeldBlockState((IBlockState) BlockState.builder().blockType(SpookyBlock).build());
            t.getWorld().spawnEntity(enderman);
            // Anna request: one hit kill
            mcEntity.setHealth(1);
            // Also Anna request: don't drop items when dead
            // This works in **SOME** cases but not others
            mcEntity.setDropItemsWhenDead(false);
            // See whether this makes a difference

            /*
            log.info("Position: " + pos);
            log.info("Rotation: " + rotation);

            log.info("Creating enderman");
             */

            log.info("Spook: Spooking " + t.getName() + " with an Enderman holding some " + SpookyBlock.getName());
            source.sendMessage(Text.of(TextColors.GREEN, "Spook: Spooking ", TextColors.WHITE, t.getName(), TextColors.GREEN, " with an Enderman holding some ", TextColors.GOLD, SpookyBlock.getName()));
            t.sendMessage(Text.of(TextColors.GOLD, "There's something", TextColors.DARK_RED, " --near-- ", TextColors.GOLD, "you!"));

        }

        return CommandResult.success();
    }

    /* public static void main(CommandContext context) {

        Player player = context.
        SpawnEnderman(logger, player);

    } */
}
