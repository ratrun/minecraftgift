package com.minecraftonline.minecraftgift;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

public class MapFinder {
    public static ArrayList<Integer> getMapIDs(String dir) throws IOException {
        ArrayList<Integer> mapIDs = null;
        Files.list(new File(dir).toPath())
                .forEach(path -> {
                    mapIDs.add(Integer.parseInt(path.getFileName()
                            .toString()
                            .replace("map_", "")
                            .replace(".dat", "")
                    ));
                });
        return mapIDs;
    }
}