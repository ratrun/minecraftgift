package com.minecraftonline.minecraftgift;

import com.flowpowered.math.vector.Vector3d;
import com.google.inject.Inject;
import com.minecraftonline.minecraftgift.spook.Spook;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandManager;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.dispatcher.SimpleDispatcher;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.entity.MainPlayerInventory;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.extent.Extent;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Plugin(
        id = "minecraftgift",
        name = "MinecraftGift",
        description = "A plugin to replace minecraftgift.sh"
)
public class MinecraftGift {
    private void registerCommand()
    {
        CommandSpec giftCommandSpec = CommandSpec.builder()
                .description(Text.of("Run MinecraftGift"))
                .permission("minecraftgift.command.give")
                .arguments(GenericArguments.optional(GenericArguments.choicesInsensitive(Text.of("GiftList"), GiftLists.GIFT_LIST_CHOICES)))
                .executor((source, args) -> {
                    main(args);
                    return CommandResult.success();
                }).build();

        //Sponge
        CommandManager cmdService = Sponge.getCommandManager();
        cmdService.register(this, giftCommandSpec, "minecraftgift");
        SimpleDispatcher rootCommand = new SimpleDispatcher();

        CommandSpec spookCommandSpec = CommandSpec.builder()
                .description(Text.of("Spook a player!"))
                .permission("minecraftgift.command.spook")
                .arguments(GenericArguments.player(Text.of("Player")))
                // Soon.
                // .arguments(GenericArguments.string(Text.of("BlockType")))
                .executor((source, args) -> {
                    Spook.SpawnEnderman(source, args);
                    return CommandResult.success();
                }).build();
        cmdService.register(this, spookCommandSpec, "spook");

        CommandSpec probabilityCommandSpec = CommandSpec.builder()
                .description(Text.of("Generate the probabilities within a gift list"))
                .permission("minecraftgift.command.probability")
                .arguments(GenericArguments.optional(GenericArguments.choicesInsensitive(Text.of("GiftList"), GiftLists.GIFT_LIST_CHOICES)))
                .executor((source, args) -> {
                    try {
                        showProbabilities(args);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return CommandResult.success();
                }).build();
        cmdService.register(this, probabilityCommandSpec, "probability");
    }

    private final Logger log;


    private final PluginContainer pluginContainer;

    private static MinecraftGift INSTANCE;

    private final StoredBook SPOOKY_BOOK = new StoredBook("spooky_book.json");

    @Inject
    public MinecraftGift(final PluginContainer pluginContainer, final Logger log)
    {
        this.log = log;
        INSTANCE = this;
        // Load Spooky Book Json //
        this.pluginContainer = pluginContainer;
        try {
            SPOOKY_BOOK.load(this.pluginContainer);
        } catch (IOException e) {
            MinecraftGift.getLogger().error("Error while loading assets.", e);
        }
    }

    public static MinecraftGift getInstance()
    {
        return INSTANCE;
    }

    public static Logger getLogger()
    {
        return INSTANCE.log;
    }

    public StoredBook getSpookyBook() {
        return SPOOKY_BOOK;
    }

    public void showProbabilities(CommandContext args) throws IOException {
        WeightedItemDeterminer giftList = args.<WeightedItemDeterminer>getOne("GiftList").orElse(GiftLists.DEFAULT);
        String output = "";
        /*for (WeightedItemDeterminer p : GiftLists.ALL_GIFT_LISTS)
        {
            output.concat(p.getProbabilities());
        }*/
        output = giftList.getProbabilities();
        // https://www.w3schools.com/java/java_files_create.asp
        String filepath = giftList.giftListName() + ".csv";
        File probabilityOutputFile = new File(filepath);
        FileWriter writer = new FileWriter(filepath);
        writer.write(output);
        writer.close();
        // return output;
    }

    private void main(CommandContext args)
    {
        //log.info("Showing probabilities");
        //log.info(showProbabilities().toString());

        // This feels like a hack!
        ItemDeterminer potionList = args.<ItemDeterminer>getOne("PotionList").orElse(GiftLists.DEFAULT_POTION);
        // A hack for eggnog.
        /*
        final ItemDeterminer ChristmasPotionList = new ChristmasPotionList();
        final ItemDeterminer ValentinesArrowList = new ValentinesArrowList();
        */
        ItemDeterminer giftList = args.<ItemDeterminer>getOne("GiftList").orElse(GiftLists.DEFAULT);

        // Did you say another hack because Anna made you realize you missed a thing? (:
        ItemDeterminer arrowList = args.<ItemDeterminer>getOne("ArrowList").orElse(GiftLists.DEFAULT_ARROW);

        Collection<Player> players = Sponge.getServer().getOnlinePlayers();
        Sponge.getServer().getBroadcastChannel().send(giftList.giftHandoutText());

        for(Player p : players)
        {
            if (p.gameMode().get() == GameModes.SPECTATOR)
            {
                p.sendMessage(Text.of(TextColors.DARK_RED, "Spectators don't get gifts!"));
                log.info(p.getName() + "did not receive a gift because they are in Spectator Mode.");
                continue;
            }

            ItemStack giftItemStack=giftList.itemToGive();

            // Similarly hacky!
            if (giftItemStack.getType().equals(ItemTypes.POTION)) {
                // logger.debug("Potion!");
                giftItemStack=potionList.itemToGive();
            }

            // Even more hacc
            if (giftItemStack.getType().equals(ItemTypes.TIPPED_ARROW)) {
                // log.info("Tipped arrow!");
                giftItemStack=arrowList.itemToGive();
            }

            /*
            if (giftItemStack.getType().equals(ItemTypes.ARROW)) {
                log.info("Arrow!");
            }
            */

            /* if (giftList.giftListName().equals("Halloween"))
            {
                log.info("Halloween!");
                p.sendMessage(Text.of("Halloween!"));
                Spook.SpawnEnderman(p);
            } */


           /* if (giftList.giftListName().equals("Christmas")) {
                log.info("Christmas!");
            }*/

            // TODO: write some actual logic for this
            // - Ted, on the morning of the 8th of February 2022
            // adding the Valentine's stuff later than he intended to
            // because three housemates are COVIDed

            // I am cleaning up this mess - tyhdefu
            if (giftItemStack.getType() == ItemTypes.POTION && GiftLists.getPotionGiftList(giftList) != null) {
                giftItemStack = GiftLists.getPotionGiftList(giftList).itemToGive();
            }
            else if (giftItemStack.getType().equals(ItemTypes.TIPPED_ARROW) && GiftLists.getArrowGiftList(giftList) != null) {
                giftItemStack = GiftLists.getArrowGiftList(giftList).itemToGive();
            }
            else if (giftItemStack.getType() == ItemTypes.FILLED_MAP) {
                int mapID;
                if (giftList.specialMapID() > 0) {
                    mapID = giftList.specialMapID();
                }
                else
                {
                    mapID = 1;
                }
                // log.info("Map ID is " + mapID);
                // I'm not sure about the necessity of this but it's the tyh approach
                // and I don't want to question the tyh approach when it's a lot
                // smarter than the Ted approach can conceivably be in the near future.
                ItemStack map = ItemStack.of(ItemTypes.FILLED_MAP);
                map = ItemStack.builder()
                        .fromContainer(giftItemStack.toContainer()
                                .set(DataQuery.of("UnsafeDamage"), mapID))
                        .build();
                giftItemStack = map;
            }

//            else {
//                getLogger().info("ItemType is " + giftItemStack.getType());
//                getLogger().info("Gift List name is: " + giftList.giftListName());
//            }


            // Get the player's inventory earlier
            Inventory inventory = p.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class));

            if (inventory.canFit(giftItemStack))
            {
                inventory.offer(giftItemStack.copy());
            }
            else
            {
                Vector3d position = p.getPosition();
                Extent extent = p.getLocation().getExtent();
                Entity item = extent.createEntity(EntityTypes.ITEM, position);
                // Tyh said Anna would be mad if we didn't
                // item.setRawData(item.toContainer().set(DataQuery.of("UnsafeData", "Owner"), p.getName()));
                // Tyh: "See? It wasn't that bad...I'm quite impressed I did that from my head."
                // [It later transpires that this is problematic, and GRAAHHAHAHA]

                // ItemType giftItem = giftList.itemToGive();

                item.offer(Keys.REPRESENTED_ITEM, giftItemStack.createSnapshot());
                extent.spawnEntity(item);
            }
            // log.info("Giving an instance of " + giftItemStack + " to " + p.getName());
            Text itemName = giftItemStack.get(Keys.DISPLAY_NAME).orElse(Text.of(giftItemStack.getTranslation()));
            // Lazy fix
            if (giftItemStack.getType() == ItemTypes.BANNER) {
                itemName = Text.of("Banner");
            }
            else if (giftItemStack.getType() == ItemTypes.WRITTEN_BOOK) {
                // Basically strip pages otherwise we'll crash the client.
                int generation = giftItemStack.get(Keys.GENERATION).orElse(0);
                List<Text> lore = giftItemStack.get(Keys.ITEM_LORE).orElse(Collections.emptyList());
                giftItemStack = ItemStack.of(giftItemStack.getType());
                giftItemStack.offer(Keys.DISPLAY_NAME, itemName);
                giftItemStack.offer(Keys.ITEM_LORE, lore);
                giftItemStack.offer(Keys.GENERATION, generation);
            }

            p.sendMessage(Text.of(TextColors.GREEN, "Enjoy your ", TextActions.showItem(giftItemStack.createSnapshot()), itemName, "!"));
        }
        Sponge.getServer().getBroadcastChannel().send(Text.of(TextColors.GREEN,players.size(), " ", players.size()==1 ? "gift" : "gifts", " handed out."));
    }

    @Listener
    public void onPluginLoad(GameReloadEvent event) {
        log.info("Ted Minecraft Gift Plugin is reloading");
    }

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        log.info("Starting the Ted Minecraft Gift Plugin");
        log.info("Registering the Minecraft Gift command.");
        registerCommand();
    }
}
